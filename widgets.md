# Le widget que nous définissons recevra un certain nombre de propriétés du chatbot 

certaines peuvent être contrôlées par les propriétés de configuration:

1- actionProvider- nous donnons le actionProviderau widget afin d'exécuter des actions si nous en avons besoin.
2- setState- nous donnons la setStatefonction de chatbot de haut niveau au widget au cas où nous aurions besoin de manipuler l'état.
3- scrollIntoView - fonction utilitaire pour faire défiler vers le bas de la fenêtre de discussion, si nous devons ajuster la vue.
4- props- si nous définissons des accessoires dans la configuration du widget, ceux-ci seront passés au widget sous le nom de la propriété configProps.
5- state- si nous définissons un état personnalisé dans la configuration, nous pouvons le mapper au widget en utilisant la mapStateToPropspropriété