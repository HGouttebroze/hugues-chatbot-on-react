class MessageParser {
    constructor(actionProvider) {
      this.actionProvider = actionProvider;
      //this.state = state;
    }
  
    parse(message) {
      //console.log(message);

      // input new msg
      const lowercase = message.toLowerCase();

      // to get a response when user tape "bonjour"
      if (lowercase.includes("bonjour")) {
        this.actionProvider.greet(); 
      }
      // to get a response when user tape "javascript"
      if (lowercase.includes("javascript")) {
        this.actionProvider.handleJavascriptList();
      }
    }
  }
  
  export default MessageParser;
  //next, I need to inplement my greet function in ./ActionProvider