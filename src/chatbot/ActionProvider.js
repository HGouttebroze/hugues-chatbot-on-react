class ActionProvider {
    constructor(createChatBotMessage, setStateFunc, createClientMessage) {
      this.createChatBotMessage = createChatBotMessage;
      this.setState = setStateFunc;
      this.createClientMessage = createClientMessage;
    }

    // step 1: I create my greet methode
    greet = () => {
      // step 3: I create message object 
      const message = this.createChatBotMessage("Bonjour et bienvenue!")
      // step 5: I need to update the chatbot state, so I use setState function
      this.addMessageToState(message); // now, I have a response when I tape
    }

    // step 2 I create also an addMessageToState methode that will take the message
    addMessageToState = (message) => {
      // step 4: I need to preserving the previous state when I will updating 
      this.setState(prevState => ({
        ...prevState,
        messages: [...prevState.messages, message]
      }))
    }


    handleJavascriptList = () => {
      const message = this.createChatBotMessage(
        "Fantastic, I've got the following resources for you on Javascript:",
        {
          widget: "javascriptLinks",
        }
      );
  
      this.updateChatbotState(message);
    };
  
    updateChatbotState(message) {
      // NOTICE: This function is set in the constructor, and is passed in from the top level Chatbot component. The setState function here actually manipulates the top level state of the Chatbot, so it's important that we make sure that we preserve the previous state.
  
      this.setState((prevState) => ({
        ...prevState,
        messages: [...prevState.messages, message],
      }));
    }
  }
  
  export default ActionProvider;